#1 Задача: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
print('FIRST TASK')
first=10
second=30
result= first + second
print(result)
result=first-second
print(result)
result=first*second
print(result)
result=first/second
print(result)
result=first**second
print(result)
result=first//second
print(result)
result=first%second
print(result)

#2 Задача: Створіть змінну і запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.
print('SECOND TASK')
mybool = 10 > 30
print(mybool)
mybool = 10 < 30
print(mybool)
mybool = 10 == 30
print(mybool)
mybool = 10 != 30
print(mybool)
#Задача: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world".
# Виведіть на екран.
print('THIRD TASK')
str1='Hello '
str2='world'
result=str1+str2
print(result)
